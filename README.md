# Capstone
Recent advancements in deep learning, especially the application of Convolutional Neural Networks (CNNs), have guided significant progress in Facial Emotion Recognition (FER). CNNs excel at automatically extracting facial features, leading to the development of robust FER systems. Deep Convolutional Neural Networks (DCNNs) have further elevated FER capabilities, outperforming traditional CNNs. However, DCNNs still face a limitation concerning their demand for high-dimensional datasets. To address this constraint, this paper proposes an ensemble model combining Self-Cure Network (SCN) with DCNN, bolstered by transfer learning.
- [Link to Paper](https://earlham.box.com/s/su3w266tzwnk41u6yfppws3c73ekm8aq)
- [Link to Video](https://youtu.be/lDucrkou2SA)
# Data Architecture Diagram
![image](imgs/Tien - data architecture diagram-4.png)

# Package
Required packages to run the program:
- torch
- numpy
- torchvision
- torch.utils.data
- torch.nn.functional
- torchvision.models
- torch.nn
- torch.nn.functional
- argparse
- random
- matplotlib
- time
- copy
- tensorboardX
- cv2
- python3
- os

# File Information
- 2 original datasets: KDEF and FER2013 are both in `data_1`
- Output images of DCNN that will be used for SCN are in `data_2`
- Main code are in `DCNN`, `SCN`, and `SCN_DCNN`: each folder contain the code and results that are used in the paper.
- `KDEF_preprocess.py`: Preprocessing KDEF into train and test data
# Command lines
The `data_2` already has data in it, so you can also skip this step, but if you want to get your own results from DCNN, run the commands below to get output from DCNN in folder `DCNN`. The output will be stored in `data_2`, so make sure no image files are in the folder, but please keep the folder strusture:
```
  python3 DCNN.py ../data_1/FER2013 ../data_2/FER2013/
  python3 DCNN.py ../data_1/KDEF ../data_2/KDEF/
```
Run both of these command for each dataset in folder `SCN_DCNN` to get result from the proposed method:
```
  python SCN_DCNN_FER.py --margin_1=0.07
  python SCN_DCNN_KDEF.py --margin_1=0.07
```
To run the original SCN:
```
  python SCN_FER.py --margin_1=0.07
  python SCN_KDEF.py --margin_1=0.07
```
